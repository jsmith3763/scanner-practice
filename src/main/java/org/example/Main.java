package org.example;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter the amount of your bill: ");
        float bill = scanner.nextFloat();
        System.out.print("Please enter the percentage you would like to tip: ");
        float tipPercentage = scanner.nextFloat();

        float tip = bill * (tipPercentage/100);

        System.out.printf("Your tip will be: %.2f\n", tip);
        System.out.printf("Your total will be: %.2f", tip + bill);


    }
}